package com.adf.tugasakhir.model;

import java.sql.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

/**
 * Conference model.
 */

@Entity
@Table(name = "conference")
@Getter @Setter
public class Conference {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nama")
    private String nama;

    @NotNull
    @Column(name = "tanggal_mulai")
    private Date tanggalMulai;

    @NotNull
    @Column(name = "ruangan_dipakai")
    private String ruanganDipakai;

    @NotNull
    @Column(name = "abstrak")
    private String abstrak;
    
    public Conference() {
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Conference)) {
            return false;
        }
        Conference conference = (Conference) o;
        return Objects.equals(id, conference.id) && Objects.equals(nama, conference.nama) && Objects.equals(tanggalMulai, conference.tanggalMulai) && ruanganDipakai == conference.ruanganDipakai && Objects.equals(abstrak, conference.abstrak);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nama, tanggalMulai, ruanganDipakai, abstrak);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", nama='" + getNama() + "'" +
            ", tanggalMulai='" + getTanggalMulai() + "'" +
            ", ruanganDipakai='" + getRuanganDipakai() + "'" +
            ", abstrak='" + getAbstrak() + "'" +
            "}";
    }
}