# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> TODO: Rayhan Muzakki, NPM:1806241204

TODO: Write the answers related to the programming exam here.

12 Factor App:

1.Codebase:
2.Dependencies: App uses Gradle automatic build system
3.Config: Already setup for getting data from environment variables, just need to add them in
4.Backing Services: use postgreSQL
5.Build,Release,Run:already implmented in gitlab-ci.yml file
6.Processes: App can be executed in Intellij IDE
7.Port Binding: It has been already declared in application.properties file
8.Concurrency:
9.Disposability: Since it is a web,it is started when entering the website and stopped when not using the website
10.Dev/prod parity: Since it is a template the time gap would be the deadline, the personnel gap and the tool gap is determined.
11.Logs:
12.Admin Processes: already been implemented by spring boot by default
